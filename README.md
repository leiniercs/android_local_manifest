[![building status](https://gitlab.com/leiniercs/android_local_manifest/badges/lineageos/pipeline.svg)](https://gitlab.com/leiniercs/android_local_manifest/-/commits/lineageos) 

# Leinier's AOSP build

**ROM:** LineageOS

**Version:** 19.1

**Android version:** 12

**Architecture:** ARM